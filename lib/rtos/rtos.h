#pragma once
#ifndef RTOS_H
#define RTOS_H

/******************************************************************************************
 * За основу взят планировщик задач с сайта ChipEnable.ru                                 *
 * http://chipenable.ru/index.php/programming-avr/item/110-planirovschik.html             *
 *                                                                                        *
 * Доработал Шибанов Владимир aka KontAr                                                  *
 * Дата: 26.03.2014                                                                       *
 *                                                                                        *
 * Изменения:                                                                             *
 * - добавлен однократный вызов задачи                                                    *
 * - добавлено удаление задачи по имени                                                   *
 * - при повторном добавлении задачи обновляются ее переменные                            *
 * - добавлен указатель на "хвост" списка                                                 *
 * - функции РТОС скорректированы с учетом "хвоста"                                       *
 ******************************************************************************************
 * shilov, 2015.04.07									                                                    *
 * совместил с модулем милисекундных задержек на таймере				                          *
 ******************************************************************************************/

#include "main.h"

/* Maximum task number */
#define MAX_TASKS      30

/**
 * Task structure
 */
typedef struct task {
   void (*pFunc) (void);  // указатель на функцию
   u16 delay;             // задержка перед первым запуском задачи
   u16 period;            // период запуска задачи
   u08 run;               // флаг готовности задачи к запуску
} task;

/**
 * Function prototypes
 */
void RTOS_Init (void);
void RTOS_SetTask (void (*taskFunc)(void), u16 taskDelay, u16 taskPeriod);
void RTOS_DeleteTask (void (*taskFunc)(void));
void RTOS_DispatchTask (void);
void RTOS_Timer (void);

void tdelay_ms(uint16_t msek);

#endif
