#ifndef _MAIN_H_
#define _MAIN_H_

#if defined(__GNUC__)
# include <avr/io.h>
# include <avr/sleep.h>
# include <avr/interrupt.h>
#elif defined(__ICCAVR__)
# include <ioavr.h>
# include <intrinsics.h>
# include <stdint.h>
#endif

/**
 * Global defines
 */
#ifndef F_CPU
  #define F_CPU   16000000
#endif
#define TIMER0_HZ         1000
#define TIMER0_PRESCALER  64
#define	TIMER0_CNT        (0x100 - (F_CPU / TIMER0_PRESCALER / TIMER0_HZ))

/**
 * Bitwise macro
 */
#define SetBit(x,y)    do{ x |=  (1 << (y));} while(0)
#define ClrBit(x,y)    do{ x &= ~(1 << (y));} while(0)
#define InvBit(x,y)    do{(x)^=  (1 << (y));} while(0)
#define IsBit(x,y)        (x &   (1 << (y)))

#define ResBit(reg,bit)    (reg &= ~_BV(bit))
/*
 Автоматически включается avr/sfr_defs.h, кторый содержит:
	_BV(bit) === (1<<(bit))
	bit_is_set(sfr, bit)
	bit_is_clear(sfr, bit)
	loop_until_bit_is_set(sfr, bit)
	loop_until_bit_is_clear(sfr, bit)
*/

/**
 * Global Type defines
 */
typedef unsigned char      u08;
typedef unsigned short     u16;
typedef unsigned long      u32;
typedef unsigned long long u64;

typedef   signed char      s08;
typedef   signed short     s16;
typedef   signed long      s32;
typedef   signed long long s64;

/**
 * Interrupt macro
 */
static volatile u08 saveRegister;

#if defined(__GNUC__)
#define  ENABLE_INTERRUPT do{sei();}while(0)
#define DISABLE_INTERRUPT do{saveRegister = SREG; cli();}while(0)
#elif defined(__ICCAVR__)
#define  ENABLE_INTERRUPT do{__enable_interrupt();}while(0)
#define DISABLE_INTERRUPT do{saveRegister = SREG; __disable_interrupt();}while(0)
#endif
#define RESTORE_INTERRUPT do{SREG = saveRegister;}while(0)
//использовать RESTORE только после DISABLE


/**
 * Logical values
 */
#define TRUE  1
#define FALSE 0

#define HIGH  1
#define LOW   0

#define ON    1
#define OFF   0

#ifndef NULL
  #define NULL  0
#endif // NULL

/**
 * Maximum values of types
 */
#define MAX08U	255
#define MAX16U	65535
#define MAX32U	4294967295

#define MIN08S	-128
#define MAX08S	 127
#define MIN16S	-32768
#define MAX16S	 32767
#define MIN32S	-2147483648
#define MAX32S	 2147483647

/**
 * GPIO defention
 */
#define HEATER_PORT   PORTB
#define HEATER_PIN    PB1



/*--------------------------------------------------------------------------*/
#endif /* _MAIN_H_ */
