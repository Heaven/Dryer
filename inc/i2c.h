#ifndef _I2C_H_
#define _I2C_H_

#define F_TWI   400000

/* I2C Status */
typedef enum {
  I2C_Ret_OK    = 0,
  I2C_Ret_NACK  = -1,
  I2C_Ret_ERR   = -2,
  I2C_Ret_TOut  = -3
} i2c_status_t;

void I2C_Init(void);
void I2C_Close(void);
void I2C_Start(void);
void I2C_Stop(void);
i2c_status_t I2C_WriteByte(const uint8_t data);
i2c_status_t I2C_ReadByte(uint8_t *data, const uint8_t ack);

#endif /* _I2C_H_ */
