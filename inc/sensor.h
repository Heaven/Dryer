#pragma once
#ifndef _SENSOR_H_
#define _SENSOR_H_

#if defined(__GNUC__)
# include <avr/io.h>
#elif defined(__ICCAVR__)
# include <ioavr.h>
# include <intrinsics.h>
# include <stdint.h>
#endif

/* Status code */
typedef enum {
  AHT10_St_OK = 0,
  AHT10_St_Err,
  AHT10_St_Bsy
} aht10_st_t;

/* Data type */
typedef struct {
  int8_t Humidity;
  int8_t Temperature;
} aht10_t;

/* Function */
aht10_st_t AHT10_Init(void);
aht10_st_t AHT10_StartMeasure(void);
aht10_st_t AHT10_GetData(aht10_t * data);

#endif /* _SENSOR_H_ */
