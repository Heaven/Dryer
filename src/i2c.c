/****************************************************
Low Level I2C Data Transreceiveing APIs

PLEASE SEE WWW.EXTREMEELECTRONICS.CO.IN FOR DETAILED
SCHEMATICS,USER GUIDE AND VIDOES.

COPYRIGHT (C) 2008-2009 EXTREME ELECTRONICS INDIA
****************************************************/

#if defined(__GNUC__)
# include <avr/io.h>
#elif defined(__ICCAVR__)
# include <ioavr.h>
# include <intrinsics.h>
# include <stdint.h>
#endif

#include "i2c.h"

#define TWI_TIMEOUT		20
volatile uint8_t TWI_WDT;

void I2C_Init(void) {
  /* TWI / I2C, 400 kHz mode, Prescaler=1
     ((F_CPU / F_TWI) - 16) / 2
     ((16000000 / 400000) - 16) / 2 = 12 */
  TWBR = 12;

  //Enable the TWI Module
  TWCR = (1<<TWEN);
}

void I2C_Close(void) {
  //Disable the module
  TWCR = 0; //&= (~(1<<TWEN));
}


void I2C_Start(void) {
  //Put Start Condition on Bus
  TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTA);

  //Poll Till Done
  TWI_WDT = TWI_TIMEOUT;
  while (!(TWCR & (1<<TWINT))) {
    if (TWI_WDT == 0) {
      PORTB |= 0x20; // Led On
    }
  }
}

void I2C_Stop(void) {
  //Put Stop Condition on bus
  TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);

  //Wait for STOP to finish
  TWI_WDT = TWI_TIMEOUT;
  while(TWCR & (1<<TWSTO) && (TWI_WDT > 0));
}

i2c_status_t I2C_WriteByte(uint8_t data) {

  TWDR = data;

  //Initiate Transfer
  TWCR = (1<<TWEN) | (1<<TWINT);

  //Poll Till Done
  TWI_WDT = TWI_TIMEOUT;
  while (!(TWCR & (1<<TWINT))) {
    if (0 == TWI_WDT) {
      return I2C_Ret_TOut;
    }
  }

  //Check Status
  if ((TWSR & 0xF8) == 0x18 || (TWSR & 0xF8) == 0x28 || (TWSR & 0xF8) == 0x40) {
    //SLA+W Transmitted and ACK received
    //or
    //SLA+R Transmitted and ACK received
    //or
    //DATA Transmitted and ACK recived
    return I2C_Ret_OK;
  } else {
    return I2C_Ret_ERR; //Error
  }
}

i2c_status_t I2C_ReadByte(uint8_t *data,uint8_t ack)
{
  //Set up ACK
  if (ack) {
    //return ACK after reception
    TWCR |= (1<<TWEA);
  } else {
    //return NACK after reception
    //Signals slave to stop giving more data
    //usually used for last byte read.
    TWCR &= (~(1<<TWEA));
  }

  //Now enable Reception of data by clearing TWINT
  TWCR |= (1<<TWINT);

  //Wait till done
  TWI_WDT = TWI_TIMEOUT;
  while (!(TWCR & (1<<TWINT))) {
    if (0 == TWI_WDT) {
      return I2C_Ret_TOut;
    }
  }

  //Check status
  if ((TWSR & 0xF8) == 0x58 || (TWSR & 0xF8) == 0x50) {
    //Data received and ACK returned
    //	or
    //Data received and NACK returned

    //Read the data
    *data = TWDR;
    return I2C_Ret_OK;
  } else {
    return I2C_Ret_ERR;	//Error
  }
}
