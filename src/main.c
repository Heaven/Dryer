/**
 * Dryer
 */
#include "main.h"
#include "i2c.h"
#include "sensor.h"
#include "rtos/rtos.h"
#include "ssd1306xled/font6x8.h"
#include "ssd1306xled/font8x16.h"
#include "ssd1306xled/ssd1306xled.h"
#include "ssd1306xled/ssd1306xledtx.h"

/* Defines */

/* Variables */
static volatile struct {
  uint8_t newTempSet: 1;
  uint8_t AHT10state: 1;
  uint8_t rezerv:     6;
} Flag;
uint8_t TemperatureSetpoint;
aht10_t Sensor;

/* Function prototypes */
static void board_Init(void);
static void checkTemperatureSetpoint(void);
static void sensorStart(void);
static void sensorGetData(void);

int main(void) {
  /* Init all */
  board_Init();
  RTOS_Init();
  I2C_Init();
  AHT10_Init();
  tdelay_ms(40);
  ssd1306_init();

  /* Clear flags */
  Flag.newTempSet = 0;
  Flag.AHT10state = 0;

  /* Set tasks */
  RTOS_SetTask(checkTemperatureSetpoint, 10, 100);
  RTOS_SetTask(sensorStart, 50, 1000);
  RTOS_SetTask(sensorGetData, 1050, 1000);

//  ssd1306_clear();
//  ssd1306_fill2(0xff, 0x00);
  ssd1306_clear();
  ssd1306tx_init(ssd1306xled_font6x8data, 0);
  // ssd1306xled_font8x16data
  ssd1306tx_stringxy((uint8_t const *)ssd1306xled_font8x16data, 0, 0, "Hello, World! :)");
  ssd1306_setpos(0, 3);
  ssd1306tx_string("SSD1306xLED Library");
//  ssd1306_setpos(0, 3);
//  ssd1306tx_string("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[5]^_`");
  ssd1306tx_stringxy((uint8_t const *)ssd1306xled_font8x16data, 0, 5, "TINUSAUR Project");
  ssd1306_setpos(0, 7);
  ssd1306tx_string("http://tinusaur.org");
  tdelay_ms(1000);

  /* Infinity loop */
  do {
  if (Flag.newTempSet != 0) {
    Flag.newTempSet = 0;
    // do some ...
  }

    ssd1306_setpos(1, 3);
    if (Flag.AHT10state == 0) {
      ssd1306tx_numdec(Sensor.Temperature);
    } else {
      ssd1306tx_string("AHT10 error");
    }

    RTOS_DispatchTask();

    // делать нечего -- спим, ждём прерывание
#if defined(__GNUC__)
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
#elif defined(__ICCAVR__)
    MCUCR = 1<<SE;
    __sleep();
#endif
  } while (1);
}

/**
 *  S u b r o u t i n e s
 */

/**
 * @brief Initialize periphery
 */
static void board_Init(void) {
  /* power off Analog Comparator */
  ACSR = ACD;

  /* GPIO */
  DDRB = 0x22; // PB1 - Triac control, PB5 - Green LED
  DDRC = 0x30; // I2C output
  PORTB = 0x03; // close Triac, enable pull-up for D8
  PORTC = 0x3e; // I2C = 1, enable pull-up for switch
  PORTD = 0xfc; // enable pull-up for D0-D7 (Switch)

  /* Timer0 - RTOS & tdelay_ms() */
  TCCR0B = ((0<<CS02)|(1<<CS01)|(1<<CS00));
  TCNT0 = TIMER0_CNT;
  TIMSK0 |= (1<<TOIE0);

  /* Timer1 */

  /* Timer2 - refresh screen values */
  //TCCR2 = 0x00;
  //TCNT2 = 0x00;
  //TIMSK |= _BV(TOIE2);

  /* Enable Interrupts */
  ENABLE_INTERRUPT;
}

static void checkTemperatureSetpoint(void) {
  static uint8_t oldTempSet = 0;
  uint8_t pvalue;

  pvalue = ~(PIND);
  if (pvalue != 0) {
    switch (pvalue) {
    case 1<<PB0:
      TemperatureSetpoint = 55;
      break;
    case 1<<PB1:
      TemperatureSetpoint = 50;
      break;
    case 1<<PB2:
      TemperatureSetpoint = 60;
      break;
    case 1<<PB3:
      TemperatureSetpoint = 65;
      break;
    case 1<<PB4:
      TemperatureSetpoint = 70;
      break;
    case 1<<PB5:
      TemperatureSetpoint = 75;
      break;
    case 1<<PB6:
      TemperatureSetpoint = 80;
      break;
    case 1<<PB7:
      TemperatureSetpoint = 85;
      break;
    default:
      TemperatureSetpoint = 0;
    }
  } else {
    pvalue = ~(PINB) | (1<<PB0);
    if (pvalue != 0) {
      TemperatureSetpoint = 90;
    } else {
      TemperatureSetpoint = 0;
    }
  }

  if (oldTempSet != TemperatureSetpoint) {
    Flag.newTempSet = 1;
    oldTempSet = TemperatureSetpoint;
  }
}

static void sensorStart(void) {
  aht10_st_t status;

  status = AHT10_StartMeasure();

  if (status != AHT10_St_OK) {
    Flag.AHT10state = 1;
  } else {
    Flag.AHT10state = 0;
  }
}

static void sensorGetData(void) {
  aht10_st_t status;

  status = AHT10_GetData(&Sensor);

  if (status != AHT10_St_OK) {
    Flag.AHT10state = 1;
  } else {
    Flag.AHT10state = 0;
  }
}

/**
 * I n t e r r u p t s
 */
#if defined(__GNUC__)
/**
 * @brief заглушка для неиспользуемых прерываний
 */
ISR(__vector_default,ISR_NAKED) {
  reti();
}
#endif
