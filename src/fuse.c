#if defined(__GNUC__)
# include <avr/io.h>

FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

#elif defined(__ICCAVR__)
# include <ioavr.h>
# include <intrinsics.h>
#endif
